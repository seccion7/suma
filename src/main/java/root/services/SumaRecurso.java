package root.services;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
@Path("/")
public class SumaRecurso {
    @GET
    @Path("/suma")
    public String getSumaQuery(@QueryParam("numeros") String numeros){
        String ListaNumeros[] = numeros.split(",");
        Integer suma = 0;
        for (int i = 0; i < ListaNumeros.length; i++) {
            //String ListaNumero = ListaNumeros[i];
            suma = suma + Integer.parseInt(ListaNumeros[i]);
            
        }
        
    return "Lista de numeros " + numeros + " @ el resultado de la suma es: " + suma;
    }
}
